# Curso de empacotamento Debian

Crie um arquivo seu-nick.md respondendo as seguintes perguntas:

## Questionário para os alunos(as)

- Qual seu nome?
- Fernando Rodrigues de Almeida

- Qual sua idade?
- 34

- Qual sua área de formação? (não necessarimente acadêmica):
- Gestão da Tecnologia da Informação

- Qual sua área de atuação?
- Tecnico Informatica

- A Quanto tempo usa GNU/Linux?
- 8-10 anos

- Adminstra ou administrou servidores GNU/Linux?
- não

- A quanto tempo usa Debian?
- 1 anos continuo

- Qual sua distribuição preferida?
- Slackware

- Em seu computador pessoal qual sistema e distribuição principal?
- Debian e Trisquel

- Qual seu editor de texto puro preferido?
- nano

- É programador(a)? Que linguagens?
- não

- Tem experiência em compilar programas?
- não

- Já criou pacotes deb?
- não

- Já criou pacotes de alguma outra distribuição?
- não

# Curso Empacotamento Debian (30h)
# Dezembro 2021

## Objetivos

Ser capaz…
- de construir um pacote existente a partir dos fontes
- de fazer alterações em pacotes existentes
- de criar um pacote novo

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- Fluxo dos pacotes no Debian
- Bugs RFP, ITP, RFS
- Anatomia de um pacote deb
- Métodos de empacotamento
- Preparação da jaula
- Pacotes fonte
- Diretório debian
- Ferramentas de desenvolvimento
- Construção de pacotes
- Atualização de pacotes
- Debianização
- Debian tracker
- Wnpp-check
- Debian policies
- Mais estudos (gpb, sbuild)

## Atividades

- Serão 8 aulas de 1h30 aproximadamente.
- 4 Laboratórios/tira-dúvidas ≃ 4h30min cada um.
- carga horária total ≃ 30h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
1   | qua | 20h    | *** |
2   | qui | 20h    | *** |
4   | sáb | 14h    |     | ***
8   | qua | 20h    | *** |
9   | qui | 20h    | *** |
11  | sáb | 14h    |     | ***
15  | qua | 20h    | *** |
16  | qui | 20h    | *** |
18  | sáb | 14h    |     | ***
21  | ter | 20h    | *** |
22  | qua | 20h    | *** |
23  | qui | 20h    |     | ***

-------
## calendário

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
.    |  .   |  [1] |  [2] |  .  |  [[4]]
.    |  .   |  [8] |  [9] |  .  | [[11]]
.    |  .   | [15] | [16] |  .  | [[18]]
.    | [21] | [22] |[[23]]|  .  |  .

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- Envie mensagem pessoal para @kretcheu no Telegram ou por e-mail: kretcheu@kretcheu.com.br


# Curso de empacotamento Debian

Crie um arquivo seu-nick.md respondendo as seguintes perguntas:

## Questionário para os alunos(as)

- Qual seu nome?
André Luis Carvalho Moreira.

- Qual sua idade?
29.

- Qual sua área de formação? (não necessarimente acadêmica):
Engenharia de computação.

- Qual sua área de atuação?
Sistemas embarcados.

- A Quanto tempo usa GNU/Linux?
10 anos.

- Adminstra ou administrou servidores GNU/Linux?
Não.

- A quanto tempo usa Debian?
Não uso atualmente, mas usei por 2 anos e derivados do debian por 6 anos.

- Qual sua distribuição preferida?
Void linux.

- Em seu computador pessoal qual sistema e distribuição principal?
Void linux.

- Qual seu editor de texto puro preferido?
Nvim.

- É programador(a)? Que linguagens?
C, C++ e python.

- Tem experiência em compilar programas?
Sim.

- Já criou pacotes deb?
Não.

- Já criou pacotes de alguma outra distribuição?
Não.

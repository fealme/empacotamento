# Curso de empacotamento Debian

Crie um arquivo seu-nick.md respondendo as seguintes perguntas:

## Questionário para os alunos(as)

- Qual seu nome? Igson Mendes da Silva
- Qual sua idade? 37 anos
- Qual sua área de formação? Infra / Analista Devops / Programador
- Qual sua área de atuação? Analísta DEVOPS

- A Quanto tempo usa GNU/Linux?  7 anos

- Adminstra ou administrou servidores GNU/Linux? Sim

- A quanto tempo usa Debian? Usei por 3 anos, parei por um tempo e voltei a usa-lo.

- Qual sua distribuição preferida?  Debian / PopOs

- Em seu computador pessoal qual sistema e distribuição principal?  Debian e PopOs

- Qual seu editor de texto puro preferido? VIM

- É programador(a)? Que linguagens? Sim. Go / Python / Java / Rust / C#

- Tem experiência em compilar programas? Não

- Já criou pacotes deb? Nunca

- Já criou pacotes de alguma outra distribuição? Nunca


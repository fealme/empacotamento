## Links para documentação

- Criando chave gpg
https://keyring.debian.org/creating-key.html

- Debian New Maintainers' Guide
https://www.debian.org/doc/manuals/maint-guide/

- Links do João Eriberto
https://people.debian.org/~eriberto/#links
http://eriberto.pro.br/wiki/index.php?title=FAQ_empacotamento_Debian

- Debian Policy Manual
https://www.debian.org/doc/debian-policy/

- Lista de discussão
https://lists.debian.org/

## Links do curso

- Tmate
https://tmate.io/t/kretcheu/debpkg
ssh kretcheu/debpkg@nyc1.tmate.io

- Aulas
https://kretcheu.com.br/debpkg
https://jitsi.malamanhado.com.br/debpkg



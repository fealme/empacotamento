Questionário para os alunos(as)

    Qual seu nome?

Thiago Fazzio

    Qual sua idade?

        36 anos

    Qual sua área de formação? (não necessarimente acadêmica):

        Marceneiro

    Qual sua área de atuação?

        DevOps GNU/Linux

    A Quanto tempo usa GNU/Linux?

Mais de 15 anos

    Adminstra ou administrou servidores GNU/Linux?

        Sim

    A quanto tempo usa Debian?

        Pelo menos 13 anos

    Qual sua distribuição preferida?

        Debian GNU/Linux

    Em seu computador pessoal qual sistema e distribuição principal?

        Debian Sid

    Qual seu editor de texto puro preferido?

        VIM

    É programador(a)? Que linguagens?

        Sim. Shell Script, Python

    Tem experiência em compilar programas?

        Sim

    Já criou pacotes deb?

        Não

    Já criou pacotes de alguma outra distribuição?

        Não

